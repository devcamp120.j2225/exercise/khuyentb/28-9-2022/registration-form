import { Col, Form, Input, Label, Row, Button } from "reactstrap";

function RegistrationForm() {
    return (
        <Form className="container">
            <Row className="text-center m-3">
                <h1>REGISTRATION FORM</h1>
            </Row>
            <Row>
                <Col sm={6}>
                    <Row className="mt-3">
                        <Col sm={3}>
                            <Label>First Name <span className="text-danger">(*)</span></Label>
                        </Col>
                        <Col sm={9}>
                            <Input></Input>
                        </Col>
                    </Row>
                    <Row className="mt-3">
                        <Col sm={3}>
                            <Label>Last Name <span className="text-danger">(*)</span></Label>
                        </Col>
                        <Col sm={9}>
                            <Input></Input>
                        </Col>
                    </Row>
                    <Row className="mt-3">
                        <Col sm={3}>
                            <Label>Birth Day <span className="text-danger">(*)</span></Label>
                        </Col>
                        <Col sm={9}>
                            <Input></Input>
                        </Col>
                    </Row>
                    <Row className="mt-3">
                        <Col sm={3}>
                            <Label>Gender <span className="text-danger">(*)</span></Label>
                        </Col>
                        <Col sm={9}>
                            <Row>
                                <Col>
                                    <Input type="radio" name="gender" />
                                    <Label className="mx-2">Male</Label>
                                </Col>
                                <Col>
                                    <Input type="radio" name="gender" />
                                    <Label className="mx-2">Female</Label>
                                </Col>
                            </Row>

                        </Col>
                    </Row>
                </Col>
                <Col sm={6}>
                    <Row className="mt-3">
                        <Col sm={3}>
                            <Label>Passport <span className="text-danger">(*)</span></Label>
                        </Col>
                        <Col sm={9}>
                            <Input></Input>
                        </Col>
                    </Row>
                    <Row className="mt-3">
                        <Col sm={3}>
                            <Label>Email <span>(*)</span></Label>
                        </Col>
                        <Col sm={9}>
                            <Input></Input>
                        </Col>
                    </Row>
                    <Row className="mt-3">
                        <Col sm={3}>
                            <Label>Country <span className="text-danger">(*)</span></Label>
                        </Col>
                        <Col sm={9}>
                            <Input></Input>
                        </Col>
                    </Row>
                    <Row className="mt-3">
                        <Col sm={3}>
                            <Label>Region <span className="text-danger">(*)</span></Label>
                        </Col>
                        <Col sm={9}>
                            <Input></Input>
                        </Col>
                    </Row>
                </Col>
            </Row>
            <Row className="mt-3">
                <Col sm={2}>
                    <Label>Subject</Label>
                </Col>
                <Col sm={10}>
                    <Input type="textarea" rows="5"></Input>
                </Col>
            </Row>
            <Row className="mt-3">
                <Col>
                    <Button style={{ float: "right" }} color="success">Send</Button>
                    <Button style={{ float: "right", marginRight: "10px" }} color="success">Check Data</Button>
                </Col>
            </Row>
        </Form>
    )
}

export default RegistrationForm;