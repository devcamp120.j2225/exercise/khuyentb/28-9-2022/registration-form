import "bootstrap/dist/css/bootstrap.min.css"
import RegistrationForm from "./Components/RegistrationForm";

function App() {
  return (
    <div className="App">
      <RegistrationForm></RegistrationForm>
    </div>
  );
}

export default App;
